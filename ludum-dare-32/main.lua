--[[
  _     _ _                    _
 | |   (_) |__  _ __ __ _ _ __(_) ___ ___
 | |   | | '_ \| '__/ _` | '__| |/ _ | __|
 | |___| | |_) | | | (_| | |  | |  __|__ \
 |_____|_|_.__/|_|  \__,_|_|  |_|\___|___/

--]]
Class     = require "lib.hump.class"
Timer     = require "lib.hump.timer"
Camera    = require "lib.hump.camera"
Vector    = require "lib.hump.vector"
Gamestate = require "lib.hump.gamestate"
Geometry  = require "lib.geometry"
inspect   = require "lib.inspect"
            require "lib.math"
            require "lib.array"
            require "lib.table"
            require "lib.list"
            require "lib.slam"
--[[
   ____ _
  / ___| | __ _ ___ ___  ___ ___
 | |   | |/ _` / __/ __|/ _ | __|
 | |___| | (_| \__ \__ \  __|__ \
  \____|_|\__,_|___/___/\___|___/

--]]
require "entities.GraphicsManager"
require "entities.AudioManager"
require "entities.FontManager"
require "entities.drawables.Drawable"
require "entities.drawables.ExitSign"
require "entities.drawables.Killzone"
require "entities.drawables.ChargeMeter"
require "entities.drawables.Basher"
require "entities.drawables.Bashers"
require "entities.drawables.Maze"
require "entities.drawables.physables.Physable"
require "entities.drawables.physables.Drone"
require "entities.drawables.physables.HollowBox"
require "entities.drawables.physables.EdgeList"
--[[
   ____                          _         _
  / ___| __ _ _ __ ___   ___ ___| |_  __ _| |_  ___ ___
 | |  _ / _` | '_ ` _ \ / _ | __| __|/ _` | __|/ _ | __|
 | |_| | (_| | | | | | |  __|__ \ |_| (_| | |_|  __|__ \
  \____|\__,_|_| |_| |_|\___|___/\__|\__,_|\__|\___|___/

--]]
require "gamestates.Menu"
require "gamestates.Game"
require "gamestates.GamePause"
require "gamestates.Win"

function love.load()
    math.randomseed(os.time())
    math.random()
    math.random()
    math.random()
    love.graphics.setBackgroundColor(54, 50, 74)
    
    Gamestate.fontManager = FontManager("/content/fonts/")
    Gamestate.fontManager:load("Traceroute.ttf", "Numbers", 120)
    Gamestate.fontManager:load("YourComplexBRK.ttf", "SmallText", 32)
    Gamestate.fontManager:load("YourComplexBRK.ttf", "Text", 60)
    Gamestate.fontManager:load("YourComplexBRK.ttf", "LargeText", 120)
    
    Gamestate.registerEvents()
    Gamestate.switch(MenuState)
end

print("lol")