MenuState = {}

function MenuState:draw()
    love.graphics.setColor(26, 23, 32, 200)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(Gamestate.fontManager.Numbers)
    love.graphics.printf("THUMP", 0, 180, 1280, "center")
    love.graphics.setFont(Gamestate.fontManager.Text)
    love.graphics.printf("survive the onslaught", 0, 300, 1280, "center")
    love.graphics.setFont(Gamestate.fontManager.SmallText)
    love.graphics.printf("use the ARROWS or WASD to THUMP", 0, 400, 1280, "center")
    love.graphics.printf("press RETURN to continue", 0, 440, 1280, "center")
    love.graphics.printf("press ESCAPE to pause", 0, 480, 1280, "center")
end

function MenuState:keypressed(key, isRepeat)
    if key == "escape" then
        love.event.push("quit")
    end
    
    if key == "return" then
        Gamestate.switch(GameState)
    end
end