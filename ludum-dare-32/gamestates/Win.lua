Win = {}

function Win:enter(previous, killed, freed)
    self.killed = killed
    self.freed = freed
end

function Win:draw()
    love.graphics.setColor(101, 147, 79, 255)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(Gamestate.fontManager.Numbers)
    love.graphics.printf("NICE", 0, 180, 1280, "center")
    love.graphics.setFont(Gamestate.fontManager.Text)
    love.graphics.printf("you survived the onslaught", 0, 300, 1280, "center")
    love.graphics.setFont(Gamestate.fontManager.SmallText)
    love.graphics.printf(string.format("you DEALT with %03d, and FREED %03d", self.killed, self.freed), 0, 420, 1280, "center")
    love.graphics.printf("press ESCAPE to quit", 0, 460, 1280, "center")
end

function Win:keypressed(key, isRepeat)
    if key == "escape" then
        love.event.push("quit")
    end
end