GameState = {}

local width = 1280
local height = 720

function GameState:init()
    self.drawables = List()
    
    self.graphicsManager = GraphicsManager("/content/gfx/")
    self.audioManager = AudioManager("/content/sfx/")
    self.fontManager = FontManager("/content/fonts/")
    
    self.graphicsManager:load("background.png", "Background")
    self.graphicsManager:load("exitMan.png", "ExitMan")
    self.graphicsManager:load("exitManGlow.png", "ExitManGlow")
    self.graphicsManager:load("basherBase.png", "BasherBase")
    self.graphicsManager:load("basherHead.png", "BasherHead")
    self.graphicsManager:load("basherPinion.png", "BasherPinion")
    self.graphicsManager:load("springTop.png", "SpringTop")
    self.graphicsManager:load("springBottom.png", "SpringBottom")
    self.graphicsManager:load("droneBody.png", "DroneBody")
    self.graphicsManager:load("droneFace.png", "DroneFace")
    self.graphicsManager:load("droneBooster.png", "DroneBooster")
    self.graphicsManager:load("chargeMeterBase.png", "ChargeMeterBase")
    self.graphicsManager:load("chargeMeterBlip.png", "ChargeMeterBlip")
    self.graphicsManager:load("mazeBackground.png", "MazeBackground")
    
    self.audioManager:load("ratchet.wav", "Ratchet")
    self.audioManager.Ratchet:setVolume(0.5)
    
    self.audioManager:load("charge.wav", "Charge")
    self.audioManager.Charge:setLooping(true)
    
    self.audioManager:load("chargeIdle.wav", "ChargeIdle")
    self.audioManager.ChargeIdle:setVolume(0.2)
    
    self.audioManager:load("chargeReady.wav", "ChargeReady")
    self.audioManager.ChargeReady:setVolume(0.7)
    
    self.audioManager:load("droneDeath.wav", "DroneDeath")
    self.audioManager:load("droneEscape.wav", "DroneEscape")
    
    self.audioManager:load({
            "metalHit/1.wav",
            "metalHit/2.wav",
            "metalHit/3.wav"
        }, "Bash")
    
    self.gfxBackground = self.graphicsManager.Background
    
    self.pendingBashX = 0
    self.pendingBashY = 0
    local bashImpact = function()
        self.cameraShakeAmount = self.cameraShakeAmount + 0.15
        self.maze:impact(self.pendingBashX, self.pendingBashY)
    end
    
    self.leftBashers = Bashers(392, 360, 0, 92, self.graphicsManager, self.audioManager, bashImpact)
    self.leftBashers:addBasher(0, -100)
    self.leftBashers:addBasher(0, 100)
    self.drawables:pushright(self.leftBashers)
    
    self.rightBashers = Bashers(888, 360, math.tau * 0.5, 92, self.graphicsManager, self.audioManager, bashImpact)
    self.rightBashers:addBasher(0, -100)
    self.rightBashers:addBasher(0, 100)
    self.drawables:pushright(self.rightBashers)
    
    self.upBashers = Bashers(640, 112, math.tau * 0.25, 92, self.graphicsManager, self.audioManager, bashImpact)
    self.upBashers:addBasher(-100, 0)
    self.upBashers:addBasher(100, 0)
    self.drawables:pushright(self.upBashers)
    
    self.downBashers = Bashers(640, 608, math.tau * 0.75, 92, self.graphicsManager, self.audioManager, bashImpact)
    self.downBashers:addBasher(-100, 0)
    self.downBashers:addBasher(100, 0)
    self.drawables:pushright(self.downBashers)
    
    self.maze = Maze(392, 112, 496, 496, math.random(60, 110), self.graphicsManager, self.audioManager)
    self.drawables:pushright(self.maze)
    
    self.chargeMeter = ChargeMeter(1060, 224, self.graphicsManager, self.audioManager)
    self.drawables:pushright(self.chargeMeter)
    
    self.camera = Camera(640, 360)
    self.cameraShakeAmount = 0
    
    love.audio.setPosition(640, 360)
end

function GameState:update(dt)
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable:update(dt)
    end
    
    if self.cameraShakeAmount > 0 then
        self.cameraShakeAmount = self.cameraShakeAmount - (self.cameraShakeAmount * dt) * 4
    else
        self.cameraShakeAmount = 0
    end
    
    self.camera:rotateTo((math.random() - 0.5) * (self.cameraShakeAmount / 8))
    self.camera:lookAt(
        640 + (math.random() - 0.5) * (self.cameraShakeAmount * 40),
        360 + (math.random() - 0.5) * (self.cameraShakeAmount * 40))
end

function GameState:draw()
    self.camera:attach()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(self.gfxBackground, 280, 0)
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(Gamestate.fontManager.Numbers)
    love.graphics.printf(string.format("%03d", self.maze.left), 20, 40, 240, "right")
    love.graphics.printf(string.format("%03d", self.maze.killed), 20, 250, 240, "right")
    love.graphics.printf(string.format("%03d", self.maze.freed), 20, 490, 240, "right")
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(Gamestate.fontManager.Text)
    love.graphics.printf("approaching", 20, 150, 240, "right")
    love.graphics.printf("euthanized", 20, 360, 240, "right")
    love.graphics.printf("liberated", 20, 600, 240, "right")
    
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable:draw()
    end
    
    self.camera:detach()
end

function GameState:keypressed(key, isRepeat)
    if key == "escape" then
        Gamestate.push(GamePauseState)
    end
    
    if
        key == "w" or key == "a" or key == "s" or key == "d" or
        key == "up" or key == "left" or key == "down" or key == "right" then
        local dischargeAmount = self.chargeMeter:discharge()
        if dischargeAmount ~= -1 then
            if (key == "s" or key == "down") and not self.upBashers.isCoolingDown then
                self.upBashers:bash()
                self.pendingBashX = 0
                self.pendingBashY = -1000 * dischargeAmount
            end

            if (key == "d" or key == "right") and not self.leftBashers.isCoolingDown then
                self.leftBashers:bash()
                self.pendingBashX = -1000 * dischargeAmount
                self.pendingBashY = 0
            end

            if (key == "w" or key == "up") and not self.downBashers.isCoolingDown then
                self.downBashers:bash()
                self.pendingBashX = 0
                self.pendingBashY = 1000 * dischargeAmount
            end

            if (key == "a" or key == "left") and not self.rightBashers.isCoolingDown then
                self.rightBashers:bash()
                self.pendingBashX = 1000 * dischargeAmount
                self.pendingBashY = 0
            end
        end
    end
end