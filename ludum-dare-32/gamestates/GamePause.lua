GamePauseState = {}

function GamePauseState:enter(parent)
    self.parent = parent
    love.audio.pause()
end

function GamePauseState:leave()
    love.audio.resume()
end

function GamePauseState:draw()
    self.parent:draw()
    
    love.graphics.setColor(26, 23, 32, 200)
    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setFont(Gamestate.fontManager.LargeText)
    love.graphics.printf("paused", 0, 300, 1280, "center")
end

function GamePauseState:keypressed(key, isRepeat)
    if key == "escape" then
        Gamestate.pop()
    end
end