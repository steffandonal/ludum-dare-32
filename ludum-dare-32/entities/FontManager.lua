FontManager = Class{}

function FontManager:init(root)
    self.fontRoot = root
end

function FontManager:load(path, name, size)
    self[name] = love.graphics.newFont(self.fontRoot .. path, size)
end