GraphicsManager = Class{}

function GraphicsManager:init(root)
    self.graphicsRoot = root
end

function GraphicsManager:load(path, name)
    self[name] = love.graphics.newImage(self.graphicsRoot .. path)
end