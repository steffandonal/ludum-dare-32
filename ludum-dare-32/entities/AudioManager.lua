AudioManager = Class{}

function AudioManager:init(root)
    self.audioRoot = root
end

function AudioManager:load(audio, name)
    if type(audio) == "table" then
        for i = 1, #audio do
            audio[i] = self.audioRoot .. audio[i]
        end
        self[name] = love.audio.newSource(audio, "static")
    else
        self[name] = love.audio.newSource(self.audioRoot .. audio, "static")
    end
end