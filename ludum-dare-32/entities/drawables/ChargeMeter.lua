ChargeMeter = Class{__includes = Drawable}

local blipInterval = -24
local minPitch = 0.2
local maxPitch = 1
local minVolume = 0
local maxVolume = 0.1
local chargeSpeed = 1 / 3
local acceptableCharge = 0.65
local idleTime = 1 / 1.7

function ChargeMeter:init(x, y, graphicsManager, audioManager)
    self.position = Vector(x, y)
    
    self.gfxBase = graphicsManager.ChargeMeterBase
    self.gfxBlip = graphicsManager.ChargeMeterBlip
    
    self.sfxCharge = audioManager.Charge
    self.sfxChargeReady = audioManager.ChargeReady
    self.sfxChargeIdle = audioManager.ChargeIdle
    
    self.chargeSound = self.sfxCharge:play()
    self.chargeSound:setPosition(640, self.position.y + 136)
    
    self.idleTime = 0
    
    self.chargeLevel = 0
end

function ChargeMeter:discharge()
    if self.chargeLevel > acceptableCharge then
        local oldChargeLevel = self.chargeLevel
        self.chargeLevel = 0
        self.chargeSound:resume()
        return oldChargeLevel
    end
    return -1
end

function ChargeMeter:update(dt)
    if self.chargeLevel < 1 then
        self.chargeLevel = self.chargeLevel + chargeSpeed * dt
        
        if self.chargeLevel >= 1 then
            local soundInstance = self.sfxChargeReady:play()
            soundInstance:setPosition(640, self.position.y + 136)
            
            self.idleTime = 1
        end
    else
        self.chargeLevel = 1
        self.chargeSound:pause()
        
        self.idleTime = self.idleTime - idleTime * dt
        if self.idleTime < 0 then
            local soundInstance = self.sfxChargeIdle:play()
            soundInstance:setPosition(640, self.position.y + 136)
            
            self.idleTime = 1
        end
    end
    
    self.chargeSound:setPitch(minPitch + (maxPitch - minPitch) * self.chargeLevel)
    self.chargeSound:setVolume(minVolume + (maxVolume - minVolume) * self.chargeLevel)
end

function ChargeMeter:draw()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(self.gfxBase, self.position.x, self.position.y)
    
    local meterColor = {r = 255, g = 207, b = 14}
    if self.chargeLevel > acceptableCharge then
        meterColor = {r = 182, g = 255, b = 14}
    end
    
    love.graphics.setColor(meterColor.r, meterColor.g, meterColor.b, 255)
    
    local currentBlipY = 212
    for i = 1, math.floor(self.chargeLevel * 8) do
        love.graphics.draw(self.gfxBlip, self.position.x + 32, self.position.y + currentBlipY)
        currentBlipY = currentBlipY + blipInterval
    end
    
    if self.chargeLevel ~= 1 then
        love.graphics.setColor(meterColor.r, meterColor.g, meterColor.b, math.floor(3 * (self.chargeLevel * 8 - math.floor(self.chargeLevel * 8))) * 85.33)
        love.graphics.draw(self.gfxBlip, self.position.x + 32, self.position.y + currentBlipY)
    end
end