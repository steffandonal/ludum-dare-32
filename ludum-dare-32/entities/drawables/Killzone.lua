Killzone = Class{__includes = Drawable}

function Killzone:init(points, drones)
    self.points = points
    self.unpackedPoints = {}
    
    for i = 1, #self.points do
        self.unpackedPoints[#self.unpackedPoints+1] = self.points[i].x
        self.unpackedPoints[#self.unpackedPoints+1] = self.points[i].y
    end
    
    self.glowAlpha = 255
    
    self.baseAlphaPulseSpeed = math.pi * (1 + math.random() / 4)
    self.baseAlphaMultiplier = 0
    self.baseAlphaSinPos = 0
    
    self.drones = drones
end

function Killzone:update(dt)
    self.glowAlpha = math.random(90, 180)
    
    if self.baseAlphaSinPos < math.pi then
        self.baseAlphaSinPos = self.baseAlphaSinPos + (self.baseAlphaPulseSpeed * dt)
    else
        self.baseAlphaSinPos = self.baseAlphaSinPos - math.pi
    end
    
    self.baseAlphaMultiplier = 0.5 + math.sin(self.baseAlphaSinPos) / 2
    
    for currentDrone in self.drones:iterleft() do
        if currentDrone.isShocked and Geometry.isPointWithinPolygon(self.points, currentDrone.position.x, currentDrone.position.y) then
            currentDrone:kill()
        end
    end
end

function Killzone:draw()
    love.graphics.setLineWidth(1)
    love.graphics.setLineStyle("smooth")
    
    love.graphics.setColor(167, 59, 128, 128 * self.baseAlphaMultiplier)
    love.graphics.polygon("fill", self.unpackedPoints)
    love.graphics.setColor(213, 59, 128, self.glowAlpha)
    love.graphics.polygon("line", self.unpackedPoints)
end