Maze = Class{__includes = Drawable}

local droneSpawnTime = 1
local droneSpawnVariance = 2

function Maze:init(x, y, width, height, numDrones, graphicsManager, audioManager)
    love.physics.setMeter(16)
    
    self.drawables = List()
    self.floatingDrawables = List()
    self.drones = List()
    
    self.position = Vector(x, y)
    self.width = width
    self.height = height
    
    self.left = numDrones
    self.killed = 0
    self.freed = 0
    
    self.graphicsManager = graphicsManager
    self.audioManager = audioManager
    
    self.gfxBackground = self.graphicsManager.MazeBackground
    
    self.world = love.physics.newWorld(0, 0, false)
    
    self.gravity = Vector(0, 0)
    self.gravitySpring = Vector(0, 0)
    
    self.container = HollowBox(self.world, 0, 0, self.width, self.height)
    
    self.drawables:pushright(Killzone({
        Vector(8, 104),
        Vector(8, 120),
        Vector(40, 120),
        Vector(40, 104)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(120, 56),
        Vector(120, 72),
        Vector(152, 72),
        Vector(152, 56)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(168, 88),
        Vector(168, 72),
        Vector(200, 72),
        Vector(200, 88)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(24, 152),
        Vector(56, 152),
        Vector(56, 168),
        Vector(24, 168)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(152, 168),
        Vector(168, 152),
        Vector(184, 168),
        Vector(168, 184)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(360, 216),
        Vector(392, 232),
        Vector(392, 248),
        Vector(344, 248),
        Vector(344, 216)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(424, 200),
        Vector(424, 168),
        Vector(456, 168),
        Vector(456, 216),
        Vector(440, 216)
    }, self.drones))
    
    self.drawables:pushright(Killzone({
        Vector(392, 40),
        Vector(392, 56),
        Vector(424, 56),
        Vector(424, 40)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(392, 72),
        Vector(424, 72),
        Vector(424, 104),
        Vector(392, 88)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(264, 120),
        Vector(264, 136),
        Vector(296, 136),
        Vector(296, 120)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(360, 264),
        Vector(360, 248),
        Vector(392, 248),
        Vector(392, 264)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(360, 296),
        Vector(360, 312),
        Vector(392, 312),
        Vector(392, 296)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(392, 344),
        Vector(392, 328),
        Vector(408, 328),
        Vector(408, 344)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(376, 360),
        Vector(376, 376),
        Vector(392, 376),
        Vector(392, 360)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(296, 376),
        Vector(280, 360),
        Vector(296, 344),
        Vector(312, 360)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(424, 424),
        Vector(440, 408),
        Vector(456, 424),
        Vector(440, 440)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(168, 440),
        Vector(184, 440),
        Vector(184, 424),
        Vector(168, 424)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(72, 360),
        Vector(56, 376),
        Vector(40, 360),
        Vector(56, 344)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(72, 296),
        Vector(56, 280),
        Vector(72, 264),
        Vector(88, 280)
    }, self.drones))

    self.drawables:pushright(Killzone({
        Vector(120, 280),
        Vector(120, 264),
        Vector(152, 264),
        Vector(152, 280)
    }, self.drones))
    
    -- Centre cage
    self.drawables:pushright(EdgeList(self.world,
        --Top
        { a = Vector(216, 232), b = Vector(232, 216) },
        { a = Vector(232, 216), b = Vector(264, 216) },
        { a = Vector(264, 216), b = Vector(280, 232) },

        --Bottom
        { a = Vector(216, 264), b = Vector(232, 280) },
        { a = Vector(232, 280), b = Vector(264, 280) },
        { a = Vector(264, 280), b = Vector(280, 264) }
    ))
    
    self.drawables:pushright(EdgeList(self.world,
        --Left
        { a = Vector(184, 216), b = Vector(184, 280) },
        { a = Vector(184, 280), b = Vector(216, 312) },
        { a = Vector(184, 216), b = Vector(216, 184) },

        --Right
        { a = Vector(312, 216), b = Vector(312, 280) },
        { a = Vector(312, 280), b = Vector(280, 312) },
        { a = Vector(312, 216), b = Vector(280, 184) },

        --Dividers
        { a = Vector(248, 280), b = Vector(248, 496) },
        { a = Vector(248, 216), b = Vector(248, 0) },
        { a = Vector(312, 248), b = Vector(496, 248) },
        { a = Vector(184, 248), b = Vector(0, 248) }
    ))
    
    self.drawables:pushright(EdgeList(self.world,
        { a = Vector(248, 168), b = Vector(216, 136) },
        { a = Vector(216, 136), b = Vector(184, 168) },
        { a = Vector(184, 168), b = Vector(168, 152) },
        { a = Vector(168, 152), b = Vector(152, 168) },
        { a = Vector(152, 168), b = Vector(168, 184) },
        { a = Vector(168, 184), b = Vector(152, 200) },
        { a = Vector(184, 216), b = Vector(152, 248) },
        { a = Vector(120, 216), b = Vector(88, 184) },
        { a = Vector(152, 200), b = Vector(120, 168) },
        { a = Vector(104, 248), b = Vector(56, 200) },
        { a = Vector(56, 200), b = Vector(0, 200) },
        { a = Vector(88, 184), b = Vector(88, 168) },
        { a = Vector(120, 168), b = Vector(120, 152) },
        { a = Vector(88, 168), b = Vector(72, 152) },
        { a = Vector(120, 152), b = Vector(152, 120) },
        { a = Vector(104, 120), b = Vector(136, 88) },
        { a = Vector(136, 88), b = Vector(168, 88) },
        { a = Vector(168, 88), b = Vector(168, 72) },
        { a = Vector(168, 72), b = Vector(200, 72) },
        { a = Vector(200, 72), b = Vector(200, 88) },
        { a = Vector(200, 88), b = Vector(216, 88) },
        { a = Vector(152, 88), b = Vector(152, 96) },
        { a = Vector(152, 120), b = Vector(216, 120) },
        { a = Vector(216, 120), b = Vector(248, 104) },
        { a = Vector(216, 88), b = Vector(216, 72) },
        { a = Vector(216, 72), b = Vector(200, 56) },
        { a = Vector(248, 56), b = Vector(200, 8) },
        { a = Vector(200, 56), b = Vector(152, 56) },
        { a = Vector(152, 56), b = Vector(152, 72) },
        { a = Vector(152, 72), b = Vector(120, 72) },
        { a = Vector(120, 72), b = Vector(120, 56) },
        { a = Vector(120, 56), b = Vector(72, 56) },
        { a = Vector(200, 8), b = Vector(200, 0) },
        { a = Vector(104, 120), b = Vector(40, 120) },
        { a = Vector(40, 120), b = Vector(40, 104) },
        { a = Vector(40, 104), b = Vector(8, 104) },
        { a = Vector(72, 152), b = Vector(56, 152) },
        { a = Vector(56, 152), b = Vector(56, 168) },
        { a = Vector(56, 168), b = Vector(24, 168) },
        { a = Vector(24, 168), b = Vector(24, 152) },
        { a = Vector(8, 104), b = Vector(8, 120) },
        { a = Vector(8, 120), b = Vector(0, 120) },
        { a = Vector(72, 56), b = Vector(56, 72) },
        { a = Vector(56, 72), b = Vector(0, 72) }
    ))
    
    self.drawables:pushright(EdgeList(self.world,
        { a = Vector(280, 184), b = Vector(328, 184) },
        { a = Vector(328, 184), b = Vector(344, 216) },
        { a = Vector(344, 216), b = Vector(344, 248) },
        { a = Vector(248, 152), b = Vector(264, 136) },
        { a = Vector(264, 136), b = Vector(296, 136) },
        { a = Vector(296, 136), b = Vector(312, 152) },
        { a = Vector(312, 152), b = Vector(328, 152) },
        { a = Vector(328, 152), b = Vector(376, 168) },
        { a = Vector(376, 168), b = Vector(392, 200) },
        { a = Vector(392, 248), b = Vector(392, 232) },
        { a = Vector(344, 216), b = Vector(360, 216) },
        { a = Vector(424, 232), b = Vector(440, 216) },
        { a = Vector(440, 168), b = Vector(408, 136) },
        { a = Vector(440, 216), b = Vector(456, 216) },
        { a = Vector(456, 216), b = Vector(456, 168) },
        { a = Vector(456, 168), b = Vector(440, 168) },
        { a = Vector(440, 216), b = Vector(424, 200) },
        { a = Vector(424, 232), b = Vector(392, 232) },
        { a = Vector(440, 168), b = Vector(424, 168) },
        { a = Vector(376, 136), b = Vector(328, 120) },
        { a = Vector(328, 120), b = Vector(312, 104) },
        { a = Vector(296, 120), b = Vector(296, 136) },
        { a = Vector(264, 120), b = Vector(264, 136) },
        { a = Vector(264, 120), b = Vector(248, 104) },
        { a = Vector(312, 104), b = Vector(280, 88) },
        { a = Vector(280, 88), b = Vector(296, 72) },
        { a = Vector(296, 72), b = Vector(344, 72) },
        { a = Vector(344, 40), b = Vector(360, 56) },
        { a = Vector(344, 40), b = Vector(296, 40) },
        { a = Vector(248, 40), b = Vector(288, 0) },
        { a = Vector(360, 56), b = Vector(376, 88) },
        { a = Vector(344, 72), b = Vector(360, 120) },
        { a = Vector(360, 120), b = Vector(408, 136) },
        { a = Vector(456, 184), b = Vector(496, 184) },
        { a = Vector(456, 72), b = Vector(456, 136) },
        { a = Vector(456, 136), b = Vector(424, 104) },
        { a = Vector(424, 104), b = Vector(424, 72) },
        { a = Vector(424, 72), b = Vector(392, 72) },
        { a = Vector(392, 72), b = Vector(392, 88) },
        { a = Vector(392, 88), b = Vector(376, 88) },
        { a = Vector(456, 72), b = Vector(440, 56) },
        { a = Vector(424, 40), b = Vector(424, 56) },
        { a = Vector(424, 56), b = Vector(392, 56) },
        { a = Vector(392, 56), b = Vector(392, 40) },
        { a = Vector(392, 40), b = Vector(344, 40) },
        { a = Vector(440, 56), b = Vector(424, 40) },
        { a = Vector(408, 0), b = Vector(408, 8) },
        { a = Vector(376, 136), b = Vector(424, 168) }
    ))
    
    self.drawables:pushright(EdgeList(self.world,
        { a = Vector(72, 496), b = Vector(72, 456) },
        { a = Vector(0, 424), b = Vector(40, 424) },
        { a = Vector(40, 424), b = Vector(88, 376) },
        { a = Vector(120, 344), b = Vector(152, 312) },
        { a = Vector(72, 456), b = Vector(120, 408) },
        { a = Vector(152, 376), b = Vector(184, 344) },
        { a = Vector(168, 360), b = Vector(152, 360) },
        { a = Vector(136, 328), b = Vector(136, 344) },
        { a = Vector(104, 424), b = Vector(88, 424) },
        { a = Vector(72, 392), b = Vector(72, 408) },
        { a = Vector(184, 344), b = Vector(184, 280) },
        { a = Vector(152, 264), b = Vector(120, 264) },
        { a = Vector(120, 264), b = Vector(120, 280) },
        { a = Vector(152, 264), b = Vector(152, 280) },
        { a = Vector(152, 280), b = Vector(184, 280) },
        { a = Vector(152, 312), b = Vector(104, 312) },
        { a = Vector(120, 280), b = Vector(88, 280) },
        { a = Vector(88, 280), b = Vector(72, 264) },
        { a = Vector(72, 264), b = Vector(56, 280) },
        { a = Vector(56, 280), b = Vector(72, 296) },
        { a = Vector(72, 296), b = Vector(56, 312) },
        { a = Vector(56, 312), b = Vector(56, 344) },
        { a = Vector(56, 344), b = Vector(40, 360) },
        { a = Vector(40, 360), b = Vector(56, 376) },
        { a = Vector(56, 376), b = Vector(72, 360) },
        { a = Vector(72, 360), b = Vector(88, 376) },
        { a = Vector(120, 344), b = Vector(104, 328) },
        { a = Vector(104, 328), b = Vector(104, 312) },
        { a = Vector(136, 408), b = Vector(136, 424) },
        { a = Vector(136, 424), b = Vector(120, 408) },
        { a = Vector(152, 392), b = Vector(168, 392) },
        { a = Vector(168, 392), b = Vector(152, 376) },
        { a = Vector(216, 440), b = Vector(200, 456) },
        { a = Vector(200, 456), b = Vector(168, 456) },
        { a = Vector(144, 496), b = Vector(120, 472) },
        { a = Vector(120, 472), b = Vector(120, 456) },
        { a = Vector(120, 456), b = Vector(136, 424) },
        { a = Vector(216, 440), b = Vector(200, 408) },
        { a = Vector(200, 408), b = Vector(200, 344) },
        { a = Vector(200, 344), b = Vector(216, 312) },
        { a = Vector(168, 456), b = Vector(168, 440) },
        { a = Vector(168, 440), b = Vector(184, 440) },
        { a = Vector(184, 440), b = Vector(184, 424) },
        { a = Vector(184, 424), b = Vector(168, 424) },
        { a = Vector(168, 424), b = Vector(168, 392) }
    ))
    
    self.drawables:pushright(EdgeList(self.world,
        { a = Vector(248, 344), b = Vector(264, 360) },
        { a = Vector(264, 360), b = Vector(280, 360) },
        { a = Vector(280, 360), b = Vector(296, 344) },
        { a = Vector(296, 344), b = Vector(344, 296) },
        { a = Vector(312, 280), b = Vector(328, 264) },
        { a = Vector(328, 264), b = Vector(360, 264) },
        { a = Vector(360, 264), b = Vector(360, 248) },
        { a = Vector(344, 296), b = Vector(360, 296) },
        { a = Vector(392, 264), b = Vector(392, 248) },
        { a = Vector(392, 264), b = Vector(424, 264) },
        { a = Vector(360, 296), b = Vector(360, 312) },
        { a = Vector(360, 312), b = Vector(392, 312) },
        { a = Vector(392, 312), b = Vector(392, 296) },
        { a = Vector(392, 296), b = Vector(424, 296) },
        { a = Vector(424, 264), b = Vector(440, 264) },
        { a = Vector(440, 264), b = Vector(456, 280) },
        { a = Vector(424, 296), b = Vector(440, 312) },
        { a = Vector(456, 280), b = Vector(472, 280) },
        { a = Vector(440, 312), b = Vector(440, 328) },
        { a = Vector(472, 280), b = Vector(496, 304) },
        { a = Vector(472, 328), b = Vector(456, 344) },
        { a = Vector(440, 328), b = Vector(424, 344) },
        { a = Vector(456, 344), b = Vector(440, 360) },
        { a = Vector(440, 360), b = Vector(408, 360) },
        { a = Vector(424, 344), b = Vector(408, 344) },
        { a = Vector(408, 344), b = Vector(408, 328) },
        { a = Vector(408, 328), b = Vector(392, 328) },
        { a = Vector(392, 328), b = Vector(392, 344) },
        { a = Vector(408, 360), b = Vector(392, 360) },
        { a = Vector(392, 360), b = Vector(392, 376) },
        { a = Vector(392, 376), b = Vector(376, 376) },
        { a = Vector(376, 376), b = Vector(376, 360) },
        { a = Vector(392, 344), b = Vector(344, 344) },
        { a = Vector(376, 360), b = Vector(344, 360) },
        { a = Vector(496, 352), b = Vector(456, 392) },
        { a = Vector(440, 360), b = Vector(392, 376) },
        { a = Vector(456, 392), b = Vector(392, 408) },
        { a = Vector(496, 424), b = Vector(456, 424) },
        { a = Vector(456, 424), b = Vector(440, 440) },
        { a = Vector(440, 472), b = Vector(440, 496) },
        { a = Vector(344, 344), b = Vector(328, 344) },
        { a = Vector(328, 344), b = Vector(312, 360) },
        { a = Vector(344, 360), b = Vector(328, 376) },
        { a = Vector(312, 360), b = Vector(296, 344) },
        { a = Vector(280, 360), b = Vector(296, 376) },
        { a = Vector(296, 376), b = Vector(280, 392) },
        { a = Vector(280, 392), b = Vector(264, 424) },
        { a = Vector(328, 376), b = Vector(296, 408) },
        { a = Vector(296, 408), b = Vector(280, 440) },
        { a = Vector(280, 440), b = Vector(280, 456) },
        { a = Vector(248, 472), b = Vector(272, 496) },
        { a = Vector(264, 424), b = Vector(248, 456) },
        { a = Vector(280, 456), b = Vector(296, 472) },
        { a = Vector(296, 472), b = Vector(392, 472) },
        { a = Vector(440, 472), b = Vector(416, 496) },
        { a = Vector(376, 376), b = Vector(328, 408) },
        { a = Vector(328, 408), b = Vector(312, 424) },
        { a = Vector(312, 424), b = Vector(312, 440) },
        { a = Vector(424, 424), b = Vector(440, 408) },
        { a = Vector(440, 408), b = Vector(456, 424) },
        { a = Vector(424, 424), b = Vector(376, 424) },
        { a = Vector(376, 424), b = Vector(392, 408) },
        { a = Vector(312, 440), b = Vector(328, 456) },
        { a = Vector(328, 456), b = Vector(392, 456) },
        { a = Vector(392, 456), b = Vector(392, 472) }
    ))
    
    self.waypointSets = self:setupWaypointses()
    
    self.floatingDrawables:pushright(ExitSign(10, 10, self.drones, self.graphicsManager))
    self.floatingDrawables:pushright(ExitSign(self.width - 41 - 10, 10, self.drones, self.graphicsManager))
    self.floatingDrawables:pushright(ExitSign(19, self.height - 54 - 10, self.drones, self.graphicsManager))
    self.floatingDrawables:pushright(ExitSign(self.width - 41 - 10, self.height - 54 - 10, self.drones, self.graphicsManager))
    
    self.spawnTime = 3
end

function Maze:impact(xvel, yvel)
    for currentDrone in self.drones:iterleft() do
        currentDrone:impact(xvel / 10, yvel / 10)
    end
    self.gravity.x = self.gravity.x - xvel
    self.gravity.y = self.gravity.y - yvel
end

function Maze:spawnDrone()
    if self.left <= 0 then return end
    self.left = self.left - 1
    self.drones:pushright(Drone(self.world, self.width / 2, self.height / 2,
            self.waypointSets[math.random(#self.waypointSets)]:copy(),
            self.graphicsManager, self.audioManager))
end

function Maze:update(dt)
    self.gravitySpring.x = self.gravitySpring.x - ((self.gravitySpring.x + self.gravity.x) / (500 * dt))
    self.gravitySpring.y = self.gravitySpring.y - ((self.gravitySpring.y + self.gravity.y) / (500 * dt))

    self.gravity.x = self.gravity.x + self.gravitySpring.x
    self.gravity.y = self.gravity.y + self.gravitySpring.y
    
    self.world:setGravity(self.gravity.x, self.gravity.y)
    self.world:update(dt)
    
    local deadDrones = {}
    for i = self.drones.head + 1, self.drones.tail do
        local currentDrone = self.drones[i]
        currentDrone:update(dt)
        if currentDrone.isDead then
            deadDrones[#deadDrones + 1] = currentDrone
        end
    end
    
    for i = 1, #deadDrones do
        self.drones:removeleft(deadDrones[i])
        
        if deadDrones[i].isFreed then
            self.freed = self.freed + 1
        else
            self.killed = self.killed + 1
        end
    end
    
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable:update(dt)
    end
    for currentFloatingDrawable in self.floatingDrawables:iterleft() do
        currentFloatingDrawable:update(dt)
    end
    
    self.spawnTime = self.spawnTime - (droneSpawnTime + math.random() * droneSpawnVariance) * dt
    if self.spawnTime < 0 then
        self:spawnDrone()
        self.spawnTime = 5
    end
    
    if self.left <= 0 and #self.drones == 0 then
        Gamestate.switch(Win, self.killed, self.freed)
    end
end

function Maze:draw()
    love.graphics.push()
    love.graphics.translate(self.position.x + self.gravity.x / 20, self.position.y + self.gravity.y / 20)
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(self.gfxBackground)
    
    for currentDrone in self.drones:iterleft() do
        currentDrone:draw()
    end
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable:draw()
    end
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setLineWidth(3)
    love.graphics.setLineStyle("smooth")
    love.graphics.rectangle("line", 0, 0, self.width, self.height)
    
    love.graphics.pop()
    
    love.graphics.push()
    love.graphics.translate(self.position.x + self.gravity.x / 40, self.position.y + self.gravity.y / 40)
        for currentFloatingDrawable in self.floatingDrawables:iterleft() do
            currentFloatingDrawable:draw()
        end
    love.graphics.pop()
end


function Maze:setupWaypointses()
    local waypointses = {}
    local waypoints = List()
    
    waypointses[1] = waypoints
    waypoints:pushright(Vector(200, 247))
    waypoints:pushright(Vector(216, 207))
    waypoints:pushright(Vector(238, 186))
    waypoints:pushright(Vector(217, 163))
    waypoints:pushright(Vector(130, 236))
    waypoints:pushright(Vector(99, 207))
    waypoints:pushright(Vector(40, 186))
    waypoints:pushright(Vector(10, 175))
    waypoints:pushright(Vector(8, 134))
    waypoints:pushright(Vector(108, 138))
    waypoints:pushright(Vector(152, 109))
    waypoints:pushright(Vector(234, 101))
    waypoints:pushright(Vector(230, 57))
    waypoints:pushright(Vector(34, 38))
    
    waypoints = List()
    waypointses[2] = waypoints
    waypoints:pushright(Vector(198, 248))
    waypoints:pushright(Vector(215, 211))
    waypoints:pushright(Vector(235, 185))
    waypoints:pushright(Vector(218, 162))
    waypoints:pushright(Vector(175, 202))
    waypoints:pushright(Vector(163, 214))
    waypoints:pushright(Vector(153, 215))
    waypoints:pushright(Vector(138, 209))
    waypoints:pushright(Vector(124, 199))
    waypoints:pushright(Vector(99, 162))
    waypoints:pushright(Vector(138, 110))
    waypoints:pushright(Vector(231, 102))
    waypoints:pushright(Vector(230, 66))
    waypoints:pushright(Vector(152, 21))
    waypoints:pushright(Vector(33, 37))
    
    waypoints = List()
    waypointses[3] = waypoints
    waypoints:pushright(Vector(294, 253))
    waypoints:pushright(Vector(292, 224))
    waypoints:pushright(Vector(261, 188))
    waypoints:pushright(Vector(271, 159))
    waypoints:pushright(Vector(359, 186))
    waypoints:pushright(Vector(394, 220))
    waypoints:pushright(Vector(410, 199))
    waypoints:pushright(Vector(362, 149))
    waypoints:pushright(Vector(260, 89))
    waypoints:pushright(Vector(274, 36))
    waypoints:pushright(Vector(348, 22))
    waypoints:pushright(Vector(465, 27))
    
    waypoints = List()
    waypointses[4] = waypoints
    waypoints:pushright(Vector(296, 250))
    waypoints:pushright(Vector(290, 218))
    waypoints:pushright(Vector(261, 182))
    waypoints:pushright(Vector(269, 160))
    waypoints:pushright(Vector(339, 173))
    waypoints:pushright(Vector(394, 218))
    waypoints:pushright(Vector(426, 214))
    waypoints:pushright(Vector(369, 148))
    waypoints:pushright(Vector(263, 96))
    waypoints:pushright(Vector(259, 70))
    waypoints:pushright(Vector(280, 61))
    waypoints:pushright(Vector(336, 59))
    waypoints:pushright(Vector(367, 88))
    waypoints:pushright(Vector(385, 117))
    waypoints:pushright(Vector(449, 161))
    waypoints:pushright(Vector(487, 134))
    waypoints:pushright(Vector(468, 38))
    
    waypoints = List()
    waypointses[5] = waypoints
    waypoints:pushright(Vector(200, 246))
    waypoints:pushright(Vector(216, 291))
    waypoints:pushright(Vector(232, 322))
    waypoints:pushright(Vector(212, 397))
    waypoints:pushright(Vector(230, 469))
    waypoints:pushright(Vector(140, 477))
    waypoints:pushright(Vector(157, 414))
    waypoints:pushright(Vector(73, 325))
    waypoints:pushright(Vector(110, 289))
    waypoints:pushright(Vector(174, 306))
    waypoints:pushright(Vector(39, 464))
    
    waypoints = List()
    waypointses[6] = waypoints
    waypoints:pushright(Vector(299, 253))
    waypoints:pushright(Vector(259, 320))
    waypoints:pushright(Vector(274, 344))
    waypoints:pushright(Vector(335, 279))
    waypoints:pushright(Vector(446, 276))
    waypoints:pushright(Vector(455, 299))
    waypoints:pushright(Vector(453, 324))
    waypoints:pushright(Vector(440, 349))
    waypoints:pushright(Vector(331, 356))
    waypoints:pushright(Vector(267, 444))
    waypoints:pushright(Vector(274, 485))
    waypoints:pushright(Vector(471, 462))
    
    waypoints = List()
    waypointses[7] = waypoints
    waypoints:pushright(Vector(295, 249))
    waypoints:pushright(Vector(289, 285))
    waypoints:pushright(Vector(261, 336))
    waypoints:pushright(Vector(343, 270))
    waypoints:pushright(Vector(460, 299))
    waypoints:pushright(Vector(485, 336))
    waypoints:pushright(Vector(399, 393))
    waypoints:pushright(Vector(328, 445))
    waypoints:pushright(Vector(468, 462))
    
    return waypointses
end