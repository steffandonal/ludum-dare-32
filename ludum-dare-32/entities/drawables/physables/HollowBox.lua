HollowBox = Class{__includes = Physable}

function HollowBox:init(world, x, y, width, height)
    Physable.init(self, world, x, y, "static")
    
    self.width = width
    self.height = height
    
    self.shapeLeft = love.physics.newEdgeShape(0, 0, 0, self.height)
    self.shapeRight = love.physics.newEdgeShape(self.width, 0, self.width, self.height)
    self.shapeTop = love.physics.newEdgeShape(0, 0, self.width, 0)
    self.shapeBottom = love.physics.newEdgeShape(0, self.height, self.width, self.height)

    self.fixtureLeft = love.physics.newFixture(self.body, self.shapeLeft);
    self.fixtureRight = love.physics.newFixture(self.body, self.shapeRight);
    self.fixtureTop = love.physics.newFixture(self.body, self.shapeTop);
    self.fixtureBottom = love.physics.newFixture(self.body, self.shapeBottom);
end

function HollowBox:draw()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.rectangle("line", self.position.x, self.position.y, self.width, self.height)
end