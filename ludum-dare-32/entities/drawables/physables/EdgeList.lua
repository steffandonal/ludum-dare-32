EdgeList = Class{__includes = Physable}

function EdgeList:init(world, ...)
    Physable.init(self, world, 0, 0, "static")
    
    self.edges = {...}
    
    for index, edge in ipairs(self.edges) do
        local shape = love.physics.newEdgeShape(edge.a.x, edge.a.y, edge.b.x, edge.b.y)
        love.physics.newFixture(self.body, shape);
    end
end

function EdgeList:draw()
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.setLineWidth(2)
    love.graphics.setLineStyle("smooth")
    
    for index, edge in ipairs(self.edges) do
        love.graphics.line(edge.a.x, edge.a.y, edge.b.x, edge.b.y)
    end
end