Physable = Class{__includes = Drawable}

function Physable:init(world, x, y, kind)
    self.world = world
    self.kind = kind
    self.position = Vector(x, y)
    
    self.body = love.physics.newBody(self.world, self.position.x, self.position.y, kind)
    
    self.isDead = false
end

function Physable:destroy()
    self.body:destroy()
    self.isDead = true
end