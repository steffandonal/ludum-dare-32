Drone = Class{__includes = Physable}

local droneSize = 6
local droneFaceOrigin = { x = 3, y = 6 }
local droneBoosterOrigin = { x = 2, y = -4 }
local waypointTolerance = 20 ^ 2
local shockVelocityThreshold = 400 ^ 2
local killTime = 1 / 0.3
local homesicknessThreshold = 5
local shockTimeRecovery = 1 / 0.5

function Drone:init(world, x, y, waypoints, graphicsManager, audioManager)
    Physable.init(self, world, x, y, "dynamic")
    
    self.rotation = 0
    self.velocity = Vector(0, 0)
    
    self.gfxBase = graphicsManager.DroneBody
    self.gfxFace = graphicsManager.DroneFace
    self.gfxBooster = graphicsManager.DroneBooster
    
    self.sfxDeath = audioManager.DroneDeath
    self.sfxEscape = audioManager.DroneEscape
    
    self.boosterGlow = 0
    self.shockTime = 0
    self.mandatoryShockTime = 0
    self.isShocked = false
    self.faceColor = { r = 0, g = 0, b = 0 }
    
    self.homesickness = 0
    
    self.isKilled = false
    self.isFreed = false
    self.killScale = 1
    
    self.shape = love.physics.newCircleShape(droneSize)
    
    self.fixture = love.physics.newFixture(self.body, self.shape, 1)
    self.fixture:setRestitution(0.3)
    self.fixture:setFriction(0)
    
    self.body:setLinearDamping(0.5)
    self.body:setInertia(0.1)
    
    self.target = Vector(0, 0)

    self.waypoints = waypoints
    self.currentWaypoint = self.waypoints:popleft()
end

function Drone:impact(xvel, yvel)
    self.body:applyLinearImpulse(xvel, yvel)
    self.isShocked = true
    self.mandatoryShockTime = 1
end

function Drone:update(dt)
    if self.isDead then return end
    
    self.position.x = self.body:getX()
    self.position.y = self.body:getY()
    self.rotation = self.body:getAngle()
    self.velocity = Vector(self.body:getLinearVelocity())
    
    if self.position:dist2(self.currentWaypoint) < waypointTolerance then
        self.waypoints:pushright(self.currentWaypoint)
        self.currentWaypoint = self.waypoints:popleft()
        self.isShocked = false
        self.homesickness = 0
    end
    
    self.homesickness = self.homesickness + dt
    if self.homesickness > homesicknessThreshold / 2 then
        self.isShocked = true
    end
    if self.homesickness > homesicknessThreshold then
        self:kill()
    end
    
    self.target = self.currentWaypoint
    
    local targetRotation = self.position:getAngleDifference(self.rotation, self.target)
    
    local targetAngle = self.position:angleTo(self.target) + math.tau * 0.25
    
    local speed = math.min(200, math.max(30, (1 - math.abs(targetRotation)) * self.position:dist(self.target)))
    self.body:applyForce(math.sin(targetAngle) * speed, -math.cos(targetAngle) * speed)
    self.body:setAngularVelocity(targetRotation * 10 + self.body:getAngularVelocity() / 1.2)
    
    self.boosterGlow = (speed - 30) / 170
     
    if self.mandatoryShockTime > 0 then
        self.mandatoryShockTime = self.mandatoryShockTime - shockTimeRecovery * dt
        self.isShocked = true
    end
    
    if self.isShocked then
        self.shockTime = self.shockTime + dt
        if self.shockTime % 0.25 < 0.125 then
            self.faceColor = {r = 159, g = 25, b = 25}
        else
            self.faceColor = {r = 255, g = 9, b = 52}
        end
    else
        self.faceColor = {r = 39, g = 39, b = 39}
    end
    
    if self.isKilled then
        self.isShocked = true
        
        self.killScale = self.killScale - killTime * dt
        if self.killScale < 0 then
            self:destroy()
        end
    end
end

function Drone:kill()
    if not self.isKilled then
        self.isKilled = true
        self.isFreed = false
        local sfxInstance = self.sfxDeath:play()
        sfxInstance:setPosition(self.position.x, self.position.y)
        sfxInstance:setAttenuationDistances(3, 10)
    end
end

function Drone:liberate()
    if not self.isKilled then
        self.isKilled = true
        self.isFreed = true
        local sfxInstance = self.sfxEscape:play()
        sfxInstance:setPosition(self.position.x, self.position.y)
        sfxInstance:setAttenuationDistances(1, 10)
    end
end

function Drone:draw()
    love.graphics.push()
    love.graphics.translate(self.position.x, self.position.y)
    
    local rotMultiple = math.tau / 8
    love.graphics.rotate(math.floor(self.rotation / rotMultiple + rotMultiple / 2) * rotMultiple)
    love.graphics.scale(self.killScale)
    
    love.graphics.setColor(255, 255, 255, 255)
    love.graphics.draw(self.gfxBase, 0, 0, 0, 1, 1, 6, 6)
    
    love.graphics.setColor(230, 60, 5, 255 * self.boosterGlow)
    love.graphics.draw(self.gfxBooster, 0, 0, 0, 1, 1, droneBoosterOrigin.x, droneBoosterOrigin.y)
    
    love.graphics.setColor(self.faceColor.r, self.faceColor.g, self.faceColor.b, 255)
    love.graphics.draw(self.gfxFace, 0, 0, 0, 1, 1, droneFaceOrigin.x, droneFaceOrigin.y)

    love.graphics.pop()
end