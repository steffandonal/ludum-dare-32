Basher = Class{__includes = Drawable}

local basherOriginX = 12
local basherOriginY = 88 / 2
local springOriginY = 16
local springWidth = 92
local headOriginY = 16
local headWidth = 8

function Basher:init(x, y, rotation, extensionLength, graphicsManager, sfxRachet)
    self.x = x
    self.y = y
    self.rotation = rotation
    self.extensionLength = extensionLength
    self.gfxBase = graphicsManager.BasherBase
    self.gfxHead = graphicsManager.BasherHead
    self.gfxPinion = graphicsManager.BasherPinion
    self.gfxSpringTop = graphicsManager.SpringTop
    self.gfxSpringBottom = graphicsManager.SpringBottom
    
    self.gfxPinion:setWrap("repeat", "repeat")
    
    self.quadPinion = love.graphics.newQuad(0, 0, self.extensionLength, 8, 8, 8)
    
    self.extensionMultiplier = 0
    self.springMultiplier = self.extensionLength / springWidth
end

function Basher:update(dt)
    self.quadPinion:setViewport(0, 0, self.extensionLength * self.extensionMultiplier, 8)
end

function Basher:draw()
    love.graphics.push()
    
    love.graphics.setColor(255, 255, 255, 255)
    
    love.graphics.translate(self.x, self.y)
    love.graphics.rotate(self.rotation)
    love.graphics.translate(-self.extensionLength - headWidth, 0)
    
    love.graphics.draw(self.gfxSpringBottom, 0, 0, 0, self.springMultiplier * self.extensionMultiplier, 0.6, 0, springOriginY)
    
    love.graphics.push()
    love.graphics.rotate(math.pi)
    love.graphics.draw(self.gfxPinion, self.quadPinion, -self.extensionLength * self.extensionMultiplier, -4)
    love.graphics.pop()
    
    love.graphics.draw(self.gfxSpringTop, 0, 0, 0, self.springMultiplier * self.extensionMultiplier, 0.6, 0, springOriginY)
    love.graphics.draw(self.gfxHead, self.extensionLength * self.extensionMultiplier, -headOriginY)

    love.graphics.draw(self.gfxBase, -basherOriginX, -basherOriginY)
    
    love.graphics.pop()
end