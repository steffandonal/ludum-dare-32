ExitSign = Class{__includes = Drawable}

local exitDistance = 30 ^ 2

function ExitSign:init(x, y, drones, graphicsManager)
    self.x = x
    self.y = y
    self.gfxBase = graphicsManager.ExitMan
    self.gfxGlow = graphicsManager.ExitManGlow
    
    self.glowAlpha = 255
    
    self.baseAlphaPulseSpeed = math.pi * (1 + math.random() / 4)
    self.baseAlphaMultiplier = 0
    self.baseAlphaSinPos = 0
    
    self.drones = drones
end

function ExitSign:update(dt)
    self.glowAlpha = math.random(30, 100)
    
    if self.baseAlphaSinPos < math.pi then
        self.baseAlphaSinPos = self.baseAlphaSinPos + (self.baseAlphaPulseSpeed * dt)
    else
        self.baseAlphaSinPos = self.baseAlphaSinPos - math.pi
    end
    
    self.baseAlphaMultiplier = 0.5 + math.sin(self.baseAlphaSinPos) / 2
    
    for currentDrone in self.drones:iterleft() do
        if currentDrone.position:dist2(Vector(self.x + 20, self.y + 27)) < exitDistance then
            currentDrone:liberate()
        end
    end
end

function ExitSign:draw()
    love.graphics.setColor(255, 255, 255, self.glowAlpha)
    love.graphics.draw(self.gfxGlow, self.x, self.y)
    love.graphics.setColor(255, 255, 255, 255 * self.baseAlphaMultiplier)
    love.graphics.draw(self.gfxBase, self.x, self.y)
end