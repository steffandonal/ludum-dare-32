Bashers = Class{__includes = Drawable}

local bashDeployTime = 1 / 0.4
local bashDeploySpeed = 13
local limitDampener = 0.8
local winchDeploySpeed = 2

function Bashers:init(x, y, rotation, extensionLength, graphicsManager, audioManager, bashCallback)
    self.drawables = List()
    
    self.x = x
    self.y = y
    self.rotation = rotation
    self.extensionLength = extensionLength
    
    self.graphicsManager = graphicsManager
    self.audioManager = audioManager
    
    self.sfxRatchet = audioManager.Ratchet
    self.sfxBash = audioManager.Bash
    
    self.bashCallback = bashCallback
    
    self.extensionMultiplier = 0
    
    self.isCoolingDown = false
    self.isBashing = false
    self.bashed = false
    self.velocity = 0
    self.deployedTime = 0
end

function Bashers:addBasher(xmod, ymod)
    self.drawables:pushright(Basher(self.x + xmod, self.y + ymod, self.rotation, self.extensionLength,self.graphicsManager))
end

function Bashers:bash()
    if not self.isCoolingDown then
        self.velocity = bashDeploySpeed
        self.isBashing = true
        self.bashed = false
        self.isCoolingDown = true
        return true
    end
    return false
end

function Bashers:update(dt)
    self.extensionMultiplier = self.extensionMultiplier + self.velocity * dt
    
    if self.extensionMultiplier > 1 then
        self.velocity = self.velocity * -limitDampener
        self.extensionMultiplier = 1
        
        if not self.bashed then
            self.bashed = true
            local sfxInstance = self.sfxBash:play()
            sfxInstance:setPosition(self.x, self.y)
            sfxInstance:setAttenuationDistances(4, 5)
            
            if self.bashCallback ~= nil then
                self.bashCallback()
            end
        end
    end
    
    if self.extensionMultiplier < 0 then
        self.velocity = 0
        self.extensionMultiplier = 0
    end
    
    if self.isBashing then
        self.velocity = self.velocity + (bashDeploySpeed * 10) * dt
        
        self.deployedTime = self.deployedTime + bashDeployTime * dt
        if self.deployedTime > 1 then
            self.deployedTime = 1
            self.isBashing = false
        end
    elseif self.isCoolingDown then
        self.velocity = self.velocity + bashDeploySpeed * dt
        
        if self.velocity > winchDeploySpeed / 2 then
            self.velocity = -winchDeploySpeed
            local sfxInstance = self.sfxRatchet:play()
            sfxInstance:setPosition(self.x, self.y)
            sfxInstance:setAttenuationDistances(1, 10)
        end
        
        if self.extensionMultiplier <= 0 then
            self.deployedTime = 0
            self.velocity = 0
            self.isCoolingDown = false
        end
    end
    
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable.extensionMultiplier = self.extensionMultiplier
        currentDrawable:update(dt)
    end
end

function Bashers:draw()
    for currentDrawable in self.drawables:iterleft() do
        currentDrawable:draw()
    end
end