function love.conf(config)
    config.identity = "ludum-dare-32"
    config.version = "0.9.2"
    config.console = false

    config.window.title = "ludum-dare-32"
    config.window.width = 1280
    config.window.height = 720
end
