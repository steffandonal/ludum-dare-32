List = Class{}

function List:init()
    self.head = 0
    self.tail = 0
end

function List:pushright(element)
    assert(element ~= nil)
    self.tail = self.tail + 1
    self[self.tail] = element
end
function List:pushleft(element)
    assert(element ~= nil)
    self[self.head] = element
    self.head = self.head - 1
end

function List:peekright()
    return self[self.tail]
end
function List:peekleft()
    return self[self.head + 1]
end

function List:popright()
    if self:isempty() then return nil end
    local element = self[self.tail]
    self[self.tail] = nil
    self.tail = self.tail - 1
    return element
end
function List:popleft()
    if self:isempty() then return nil end
    local element = self[self.head + 1]
    self.head = self.head + 1
    local element = self[self.head]
    self[self.head] = nil
    return element
end

function List:_removeatinternal(index)
    for i = index, self.tail do
        self[i] = self[i + 1]
    end
    self.tail = self.tail - 1
end

function List:removeright(index)
    for i = self.tail, self.head + 1, -1 do
        if self[i] == index then
            self:_removeatinternal(i)
            return true
        end
    end
    return false
end
function List:removeleft(index)
    for i = self.head + 1, self.tail do
        if self[i] == index then
            self:_removeatinternal(i)
            return true
        end
    end
    return false
end

function List:length()
    return self.tail - self.head
end

function List:isempty()
    return self:length() == 0
end

function List:contents()
    local result = {}
    for i = self.head + 1, self.tail do
        result[i - self.head] = self[i]
    end
    return result
end

function List:copy()
    local newList = List()
    for currentItem in self:iterleft() do
        newList:pushright(currentItem)
    end
    return newList
end

function List:iterright()
    local i = self.tail + 1
    return function()
        if i > self.head + 1 then
            i = i - 1
            return self[i]
        end
    end
end
function List:iterleft()
    local i = self.head
    return function()
        if i < self.tail then
            i = i + 1
            return self[i]
        end
    end
end