array = {}

function array.initialise2d(width, height, default)
    local newArray = {}

    for x = 1, 128 do
        newArray[x] = {}
        for y = 1, 128 do
            newArray[x][y] = default
        end
    end

    return newArray
end