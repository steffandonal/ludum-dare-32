math.tau = math.pi * 2

function math.clamp(value, lowerBound, upperBound)
    return math.min(math.max(value, lowerBound), upperBound)
end

function math.gaussianish(value)
    local height = 1
    local center = 1
    local width = 1 / 2.8

    return  height *
        math.exp( -(
            ((value - center) ^ 2)
            /
            (2 * width ^ 2)
        ))
end

function math.angle(x1,y1, x2,y2)
    return math.atan2(y2-y1, x2-x1)
end

function math.sign(x)
    if x < 0 then
        return -1
    elseif x > 0 then
        return 1
    else
        return 0
    end
end

function math.angleDifference(angleA, angleB)
    return math.wrapAngle(angleB - angleA + math.tau * 0.25)
end

function math.wrapAngle(angle)
    while angle > math.tau * 0.5 do
        angle = angle - math.tau
    end
    while angle < -math.tau * 0.5 do
        angle = angle + math.tau
    end
    
    return angle
end