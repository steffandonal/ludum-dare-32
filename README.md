# ludum-dare-32

## Dependencies
This game depends on [LÖVE](https://love2d.org/) being installed on your machine in order to run.

### Running the game from source
#### Windows
Windows users can open the command window in the root of the repository (`Shift + Right Click`, then `Open command window here`), then run `love ludum-dare-32`.

#### OSX
You can drag the `ludum-dare-32` folder from the repository root onto the LÖVE application bundle.

#### Linux
Run `love REPOPATH/ludum-dare-32` from the terminal, replacing "REPOPATH" with the place you have saved the repository.